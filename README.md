# GitLab CI
- [ci](https://about.gitlab.com/gitlab-ci/)
- [artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html)
- [docker](https://docs.gitlab.com/omnibus/docker/)